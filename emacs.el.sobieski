;; -*- emacs-lisp -*-
;; 
;; For Emacs 2x.x
;; Copyleft http://yeupou.wordpress.com

;;*********************
;; DOTFILE
;; regenerate the .gnus.elc if .gnus.el is newer
(defun dotemacscheck! ()
  "If .emacs.el exists and is newer than .emacs.elc, recompile it"
  (cond
   ((file-newer-than-file-p "~/.rc/emacs.el" "~/.rc/emacs.elc")
    (let ((mode-line-format "*** Recompiling .emacs.el ***"))
      (sit-for 1)
      (byte-compile-file "~/.rc/emacs.el")
      (message ".emacs.elc recompiled --- reloading...")
      )
    (load "~/.rc/emacs.elc" t t t)
    )))
(dotemacscheck!)


;;*******************
;; Always do UTF8
(prefer-coding-system 'utf-8)


;;*******************
;; COSMETICS

;; Cosmetics
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bold-italic ((t (:underline "cyan" :slant italic :weight bold))))
 '(custom-button-pressed-unraised ((t (:inherit custom-button-unraised :foreground "brightmagenta"))))
 '(custom-state-face ((((class color) (background dark)) (:foreground "#90ff75"))) t)
 '(escape-glyph ((t (:foreground "cyan"))))
 '(fixed-pitch ((t nil)))
 '(font-lock-builtin-face ((t (:foreground "LightSteelBlue"))))
 '(font-lock-comment-face ((t (:background "#4d4d4d" :foreground "chocolate1"))))
 '(font-lock-constant-face ((t (:foreground "Aquamarine"))))
 '(font-lock-function-name-face ((t (:foreground "LightSkyBlue"))))
 '(font-lock-keyword-face ((t (:foreground "Cyan1"))))
 '(font-lock-string-face ((t (:foreground "LightSalmon"))))
 '(font-lock-type-face ((t (:foreground "PaleGreen"))))
 '(font-lock-variable-name-face ((t (:foreground "color-227"))))
 '(fringe ((t (:background "#4d4d4d" :foreground "#b4b4b4"))))
 '(highlight ((t (:background "aquamarine" :foreground "black"))))
 '(homoglyph ((t (:foreground "cyan"))))
 '(info-xref-visited ((t (:inherit (link-visited info-xref) :background "color-246"))))
 '(italic ((t (:underline "cyan" :slant italic))))
 '(match ((t (:background "RoyalBlue3"))))
 '(region ((t (:background "#73DAE1" :foreground "black"))))
 '(variable-pitch ((t nil)))
 '(widget-field ((((class grayscale color) (background light)) (:underline "gray52")))))





;;**********************
;; SHORTCUFS, PREFS
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)

;; Selection related
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(case-fold-search t)
 '(delete-selection-mode 1)
 '(display-time-24hr-format t t)
 '(global-font-lock-mode t nil (font-lock))
 '(message-directory "~/.Mail/" t)
 '(mouse-wheel-mode t nil (mwheel))
 '(read-mail-command (quote gnus))
 '(save-place t nil (saveplace))
 '(save-place-mode t nil (saveplace))
 '(select-enable-clipboard nil)
 '(show-paren-mode t nil (paren))
 '(spell-command "aspell")
 '(text-mode-hook (quote (turn-on-auto-fill text-mode-hook-identify)))
 '(tool-bar-mode nil)
 '(transient-mark-mode t)
 '(uniquify-buffer-name-style (quote forward) nil (uniquify)))

;; Also defined in .gnus.el
;; avoid apparition of ~/Mail/.
(setq gnus-directory "~/.News/")
(setq message-directory "~/.Mail/")

;; Ignore startup kind of splash screen
(setq inhibit-startup-message t)

;; Avoid spreading ~ 
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )

;;*******************
;; MODES
;; Comment-out modes not installed on your computer.

;; Little bar cursor instead of a block. Easier for me.
;(require 'bar-cursor)
;(bar-cursor-mode 1)

;; Scroll bar to the right, by habbit, by implicit convention.
;(set-scroll-bar-mode 'right)

;; recently opened files
(require 'recentf) 
(recentf-mode 1)


;; Use templates
(require 'autoinsert)
(auto-insert-mode)
(setq auto-insert-directory "~/.rc/templates/")
(setq auto-insert-query nil)
(define-auto-insert "\.sh" "template.sh")
(define-auto-insert "\.pl" "template.pl")
(define-auto-insert "\.pm" "template.pl")
(define-auto-insert "\.tex" "template.tex")
(define-auto-insert "\.php" "template.php")
(define-auto-insert "\.css" "c-template.c")
(define-auto-insert "\.c" "c-template.c") 

;;*******************
;; FUNCTIONS

;; Word counts, stats about words
(defun word-count nil
  "Count words in the buffer or active region using wc"
  (interactive)
  (if mark-active
      (shell-command-on-region (point) (mark) "wc -w")
    (shell-command-on-region (point-min) (point-max) "wc -w")))
(defalias 'wc 'word-count)

(message "Loading ~/.emacs.el...")
;; EOF
(put 'scroll-left 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
