local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")

-- Create the volume widget
local volume_widget = wibox.widget {
    image = beautiful.widget_vol, -- Default icon from your theme
    widget = wibox.widget.imagebox
}

-- Function to update widget appearance and optionally show notification
local function update_volume_widget(show_notification, sink_name)
    awful.spawn.easy_async("wpctl get-volume @DEFAULT_SINK@", function(stdout)
        local volume_output = stdout
        local volume = string.match(volume_output, "Volume: ([0-9.]+)")
        local is_muted = volume_output:match("%[MUTED%]") ~= nil
        local volume_percent = volume and math.floor(tonumber(volume) * 100) or 0

        -- Choose an icon based on mute status (customize these paths as needed)
        local icon = is_muted and beautiful.widget_vol_mute or beautiful.widget_vol
	volume_widget:set_image(icon) -- Update the widget icon

	-- FIXME: miss the ARC widget
	
        if show_notification then
	   local notification_text
	   if sink_name then
	      -- When changing the sink, show the sink name with volume percent if not muted
	      if is_muted then
		 notification_text = sink_name
	      else
		 notification_text = sink_name .. " (" .. volume_percent .. "%)"
	      end	      
	   else
	      -- When changing the volume, show volume level or mute status
	      notification_text = is_muted and "Muted" or (volume_percent .. "%")
	   end
	   naughty.notify({
		 title = sink_name and "Active Sink" or "Volume",
		 text = notification_text,
		 icon = icon,
		 timeout = 2
	   })	      
        end
    end)
end

-- Periodic update timer
local volume_timer = gears.timer {
    timeout = 5,
    autostart = true,
    callback = function() update_volume_widget(false) end -- No notification for periodic updates
}
update_volume_widget(false) -- Initial update without notification


-- Show sinks and allow to change output
local function show_sink_menu()
    awful.spawn.easy_async("wpctl status", function(stdout)
        local sinks = {}
        local in_sinks_section = false
        local sink_lines = {}
        local max_name_length = 0

	-- Preprocess: remove special characters and normalize whitespace
        local cleaned_output = stdout:gsub("[├─│]", ""):gsub("\t", " ")

        -- Process each line of the cleaned output
        for line in cleaned_output:gmatch("[^\r\n]+") do
            if line:match("^%s*Sinks:%s*") then
                in_sinks_section = true
            elseif in_sinks_section and line:match("^%s*[*%d]") then
                table.insert(sink_lines, line)
                local is_active = line:match("^%s*[*]") ~= nil
                local id, name = line:match("^%s*[* ]?%s*(%d+)%.%s+(.-)%s+%[vol:")
                if id and name then
		   name = name:gsub("%s+$", "")
		   if #name > max_name_length then
		      max_name_length = #name -- Update longest name length
		   end		   
		   table.insert(sinks, {
				   name,
				   function()
				      awful.spawn("wpctl set-mute " .. id  .. " 0", false) -- Unmute	  
				      awful.spawn("wpctl set-default " .. id, false)
				      update_volume_widget(true, name) -- Trigger notification with sink name
				   end,
				   icon = is_active and beautiful.widget_vol or nil -- Show vol icon for active sink
		   })
                end
            elseif in_sinks_section and line:match("%s*%a+:") then
	       in_sinks_section = false
            end
        end	

        -- Error handling if no sinks are found
        if #sinks == 0 then
            local sinks_text = table.concat(sink_lines, "\n")
            naughty.notify({
                title = "Sink Error",
                text = "No sinks detected. Parsed sink lines:\n" .. (sinks_text ~= "" and sinks_text or "No lines found"),
                timeout = 10,
                preset = naughty.config.presets.critical
            })
            return
        end


        -- Create and show the menu
	awful.menu({
	      items = sinks,
        }):show()
    end)
end


-- Mouse bindings
volume_widget:buttons(gears.table.join(
    awful.button({}, 1, function() -- Left-click to toggle mute
        awful.spawn("wpctl set-mute @DEFAULT_SINK@ toggle", false)
        update_volume_widget(true) -- Show notification
    end),
    awful.button({}, 3, show_sink_menu), -- Right-click for sink menu
    awful.button({}, 4, function() -- Scroll up to increase volume
        awful.spawn("wpctl set-mute @DEFAULT_SINK@ 0", false) -- Unmute	  
        awful.spawn("wpctl set-volume @DEFAULT_SINK@ 5%+", false)
        update_volume_widget(true) -- Show notification
    end),
    awful.button({}, 5, function() -- Scroll down to decrease volume
        awful.spawn("wpctl set-mute @DEFAULT_SINK@ 0", false) -- Unmute
        awful.spawn("wpctl set-volume @DEFAULT_SINK@ 5%-", false)
        update_volume_widget(true) -- Show notification
    end)
))


-- Return the widget so it can be added to the wibar
return volume_widget
