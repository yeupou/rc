-- ~/.config/awesome/clock_widget.lua
local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")

-- Function to strip ANSI codes for width calculation
local function strip_ansi_codes(text)
    return text:gsub("\27%[[%d;]*m", "")
end

-- Function to find the longest line’s length and count lines
local function get_dimensions(output)
    local lines = gears.string.split(output, "\n")
    local max_len = 0
    local line_count = 0
    for _, line in ipairs(lines) do
        local stripped = strip_ansi_codes(line)
        if #stripped > max_len then
            max_len = #stripped
        end
        if #stripped > 0 then -- Count non-empty lines
            line_count = line_count + 1
        end
    end
    return max_len, line_count
end

-- Function to convert ANSI codes to Pango markup
local function ansi_to_pango(text)
    local result = {}
    local pos = 1
    local bold_open = false
    local color_open = false
    local inverse_open = false

    while true do
        local start_idx, end_idx, code = text:find("\27%[([%d;]*)m", pos)
        if not start_idx then
            table.insert(result, text:sub(pos))
            break
        end

        if start_idx > pos then
            table.insert(result, text:sub(pos, start_idx - 1))
        end

        if code == "1" then
            if not bold_open then
                table.insert(result, "<b>")
                bold_open = true
            end
        elseif code == "0" then
            local closing = ""
            if bold_open then
                closing = closing .. "</b>"
                bold_open = false
            end
            if color_open then
                closing = closing .. "</span>"
                color_open = false
            end
            if inverse_open then
                closing = closing .. "</span>"
                inverse_open = false
            end
            if closing ~= "" then
                table.insert(result, closing)
            end
        elseif code == "7" then
            if not inverse_open then
                table.insert(result, "<span bgcolor='#ffffff' fgcolor='#000000'>")
                inverse_open = true
            end
        elseif code:match("^38;2;(%d+);(%d+);(%d+)$") then
            local r, g, b = code:match("^38;2;(%d+);(%d+);(%d+)$")
            r, g, b = tonumber(r), tonumber(g), tonumber(b)
            if r and g and b and r <= 255 and g <= 255 and b <= 255 then
                table.insert(result, string.format('<span color="#%02x%02x%02x">', r, g, b))
                color_open = true
            end
        end

        pos = end_idx + 1
    end

    while inverse_open do
        table.insert(result, "</span>")
        inverse_open = false
    end
    while color_open do
        table.insert(result, "</span>")
        color_open = false
    end
    if bold_open then table.insert(result, "</b>") end

    return table.concat(result)
end

-- Function to create the clock widget
local function create_clock_widget(screen)
    local clock_widget = wibox.widget {
        {
            format = "%a %d %b %H:%M",
            widget = wibox.widget.textclock,
        },
        bg = beautiful.violet,
        widget = wibox.container.background
    }

    local popup = awful.popup {
        screen = screen,
        widget = wibox.widget {
            {
                id = "calendar_text",
                font = "Monospace 10",
                widget = wibox.widget.textbox,
            },
            margins = 4,
            widget = wibox.container.margin
        },
        border_width = 1,
        border_color = beautiful.bg_normal,
        bg = beautiful.bg_normal,
        shape = gears.shape.rounded_rect,
        ontop = true,
        visible = false,
    }

    local function update_popup()
        awful.spawn.easy_async("khal --color calendar", function(stdout, stderr, _, exitcode)
            if exitcode == 0 and stdout and stdout ~= "" then
                local pango_text = ansi_to_pango(stdout)
                local stripped = strip_ansi_codes(stdout)
                local max_len, line_count = get_dimensions(stripped)
                local char_width = 8
                local line_height = 16
                local new_width = math.max(300, max_len * char_width + 20)
                local new_height = line_count * line_height + 8

                -- Set dimensions before positioning
                popup.width = new_width
                popup.height = new_height

                -- Use wibar geometry for positioning
                local wibar = screen.mywibox
                if wibar then
                    local wibar_geo = wibar:geometry()
                    local widget_x_offset = wibar_geo.width - new_width - 50 -- Adjust offset as needed
                    popup.x = wibar_geo.x + widget_x_offset
                    popup.y = wibar_geo.y + wibar_geo.height + 5
                    if popup.x < wibar_geo.x then popup.x = wibar_geo.x end
                    awful.placement.no_offscreen(popup)
                else
                    -- Fallback if wibar isn’t ready
                    popup.x = screen.geometry.width - new_width - 10
                    popup.y = 10
                end

                -- Update content and show popup
                local textbox = popup.widget:get_children_by_id("calendar_text")[1]
                textbox:set_markup(pango_text)
                -- Ensure visibility after sizing and positioning
                popup.visible = true
            else
                naughty.notify({
                    title = "Clock Widget Error",
                    text = "Failed to run khal --color calendar: " .. (stderr or "No output"),
                    preset = naughty.config.presets.critical
                })
                popup.visible = false
            end
        end)
    end

    local popup_visible = false
    clock_widget:buttons(gears.table.join(
        awful.button({}, 1, function()
            if popup_visible then
                popup.visible = false
                popup_visible = false
            else
                update_popup()
                popup_visible = true
            end
        end)
    ))

    return clock_widget
end

return create_clock_widget

-- EOF 
