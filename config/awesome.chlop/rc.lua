-- ~/.config/awesome/rc.lua

os.setlocale(os.getenv("LANG"))

-- === Libraries ===
-- Standard Awesome Libraries
local gears         = require("gears")
local awful         = require("awful"); require("awful.autofocus")
local wibox         = require("wibox")
local beautiful     = require("beautiful")
local naughty       = require("naughty")
local menubar       = require("menubar")

-- Theme Setup (must be earlier than widget load)
local chosen_theme = "darkblue"
local theme_path   = string.format("%s/.config/awesome/%s/theme.lua", os.getenv("HOME"), chosen_theme)
beautiful.init(theme_path)

-- Additional Libraries
local bling         = require("bling")
local lain          = require("lain")
local scratch       = require("scratch")
local freedesktop   = require("freedesktop")
local hotkeys_popup = require("awful.hotkeys_popup").widget

-- Extra Widgets
local cpu_widget         = require("awesome-wm-widgets.cpu-widget.cpu-widget")
--local volume_widget      = require("volume_widget")  -- use pasystray instead
local logout_menu_widget = require("awesome-wm-widgets.logout-menu-widget.logout-menu")
local clock_widget       = require("clock_widget")


-- Debian Menu
require("debian.menu")

-- === Default Applications and tags
local terminal   = "xfce4-terminal"
local editor     = "emacs -nw"
local editor_cmd = terminal .. " -e " .. editor
local modkey     = "Mod4"
awful.util.terminal = terminal
awful.util.tagnames = {"📩", "📌", "3", "4", "5", "6", "7", "8", "9"}


-- === Error Handling ===
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title  = "Oops, there were errors during startup!",
        text   = awesome.startup_errors
    })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        if in_error then return end
        in_error = true
        naughty.notify({
            preset = naughty.config.presets.critical,
            title  = "Oops, an error happened!",
            text   = tostring(err)
        })
        in_error = false
    end)
end

-- Function to check syntax and restart
local function check_and_restart()
    awful.spawn.easy_async("awesome -k " .. awesome.conffile, function(stdout, stderr, exit_reason, exit_code)
        -- Check if stdout contains "syntax OK" or exit_code is 0 (success)
        if stdout:match("syntax OK") or exit_code == 0 then
            awesome.restart()
        else
            naughty.notify({
                title = "Configuration Error",
                text = "Syntax check failed:\n" .. (stdout .. "\n" .. stderr),
                timeout = 10,
                preset = naughty.config.presets.critical
            })
        end
    end)
end

-- === Wallpaper ===
local wallpaper_path = os.getenv("HOME") .. "/.config/backgrounds/"
local lfs = require("lfs") -- LuaFileSystem to list directory contents

-- Function to get a random wallpaper from the directory
local function get_random_wallpaper()
   local files = {}
   local supported_extensions = {".png", ".jpg", ".jpeg", ".svg"} -- Supported formats

    -- Populate files table with only image files
    for file in lfs.dir(wallpaper_path) do
        if file ~= "." and file ~= ".." then
            -- Check if the file ends with a supported extension (case-insensitive)
            local ext = file:lower():match("%.([^%.]+)$")
            if ext and gears.table.hasitem(supported_extensions, "." .. ext) then
                table.insert(files, file)
            end
        end
    end
    
    -- If no files are found, return nil
    if #files == 0 then
       return nil
    end
    
    -- Pick a random file
    local random_index = math.random(1, #files)
    return wallpaper_path .. files[random_index]
end

-- Function to set the wallpaper
local function set_wallpaper()
    local wallpaper = get_random_wallpaper()
    if wallpaper then
        gears.wallpaper.maximized(wallpaper, nil, false)
    else
        -- Fallback to a default wallpaper if no files are found
        gears.wallpaper.maximized(wallpaper_path .. "230818_184237.jpg", nil, false)
    end
end

-- Timer to change wallpaper every 15 minutes (900 seconds)
gears.timer {
    timeout = 900,          -- 15 minutes = 900 seconds
    call_now = true,        -- Set wallpaper immediately on startup
    autostart = true,       -- Start the timer automatically
    callback = set_wallpaper
}

-- Handle screen changes (e.g., resolution or monitor setup)
screen.connect_signal("request::wallpaper", set_wallpaper)

-- === Menu ===
local myawesomemenu = {
    { "Hotkeys",     function() return false, hotkeys_popup.show_help end },
    { "Edit config", editor_cmd .. " " .. awesome.conffile },
    { "Restart after checking syntax", check_and_restart },
    { "Restart",     awesome.restart },
    { "Quit",        function() awesome.quit() end }
}
local mymainmenu = freedesktop.menu.build({
    before = {
        { "Awesome",  myawesomemenu, beautiful.awesome_icon },
        { "Run",      function() awful.screen.focused().mypromptbox:run() end },
        { "Terminal", terminal }
    }
})
local mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })
menubar.utils.terminal = terminal

-- === Widgets ===
local temp_icon    = wibox.widget.imagebox(beautiful.widget_temp)
local temp         = lain.widget.temp({
    settings = function()
        widget:set_markup(lain.util.markup.font(beautiful.font, lain.util.markup.fg.color(beautiful.fg_widget, " " .. coretemp_now .. "° ")))
    end
})
local temp_widget  = wibox.container.background(wibox.container.margin(wibox.widget { temp_icon, temp.widget, layout = wibox.layout.align.horizontal }, 0, 0), beautiful.red)

-- === Volume handling ===
local function volume_notification()
   -- Run wpctl asynchronously
    local cmd = "wpctl get-volume @DEFAULT_SINK@"
    awful.spawn.easy_async(cmd, function(stdout, stderr, _, exit_code)
        -- Check if the command failed
        if exit_code ~= 0 then
            naughty.notify({
                title = "Volume Error",
                text = "wpctl failed: " .. tostring(stderr),
                timeout = 2,
                preset = naughty.config.presets.critical
            })
            return
        end
        
        -- Check if output is empty
        local volume_output = stdout
        if volume_output == "" then
            naughty.notify({
                title = "Volume Error",
                text = "No output from wpctl. Check PipeWire/WirePlumber.",
                timeout = 2,
                preset = naughty.config.presets.critical
            })
            return
        end
        
        -- Parse volume from output (e.g., "Volume: 0.82")
        local volume = string.match(volume_output, "Volume: ([0-9.]+)")
        if not volume then
            naughty.notify({
                title = "Volume Error",
                text = "Unable to parse volume: " .. volume_output,
                timeout = 2,
                preset = naughty.config.presets.critical
            })
            return
        end
        
        -- Convert to percentage
        local volume_percent = tonumber(volume) * 100
        if not volume_percent then
            naughty.notify({
                title = "Volume Error",
                text = "Invalid volume value: " .. volume,
                timeout = 2,
                preset = naughty.config.presets.critical
            })
            return
        end
        
        -- Check mute status
        local is_muted = volume_output:match("%[MUTED%]") ~= nil
        
        -- Set notification text and icon
        local notification_text = is_muted and "Muted" or string.format("%.0f%%", volume_percent)
        local icon
        if is_muted then
            icon = beautiful.widget_vol_mute
        elseif volume_percent == 0 then
            icon = beautiful.widget_vol_no
        elseif volume_percent < 30 then
            icon = beautiful.widget_vol_low
        else
            icon = beautiful.widget_vol
        end
        
        -- Show the notification
        naughty.notify({
            title = "Volume",
            text = notification_text,
            icon = icon,
            timeout = 2
        })
    end)
end


-- === Button Definitions ===
local taglist_buttons = awful.util.table.join(
    awful.button({}, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t) if client.focus then client.focus:move_to_tag(t) end end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t) if client.focus then client.focus:toggle_tag(t) end end),
    awful.button({}, 5, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({}, 4, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = awful.util.table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c.minimized = false
            if not c:isvisible() and c.first_tag then c.first_tag:view_only() end
            client.focus = c
            c:raise()
        end
    end),
    awful.button({}, 3, function() 
        local instance = nil
        return function() 
            if instance and instance.wibox.visible then 
                instance:hide()
                instance = nil
            else 
                instance = awful.menu.clients({ theme = { width = 250 } })
            end
        end
    end),
    awful.button({}, 4, function() awful.client.focus.byidx(1) end),
    awful.button({}, 5, function() awful.client.focus.byidx(-1) end)
)


-- === Layouts ===
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.spiral,
    awful.layout.suit.corner.nw,
    awful.layout.suit.corner.ne,
    awful.layout.suit.floating,
}

lain.layout.termfair.nmaster        = 3
lain.layout.termfair.ncol           = 1
lain.layout.termfair.center.nmaster = 3
lain.layout.termfair.center.ncol    = 1
lain.layout.cascade.tile.offset_x   = 2
lain.layout.cascade.tile.offset_y   = 32
lain.layout.cascade.tile.extra_padding = 5
lain.layout.cascade.tile.nmaster    = 5
lain.layout.cascade.tile.ncol       = 2

-- === Notifications ===
naughty.config.defaults.timeout      = 5
naughty.config.defaults.screen       = 1
naughty.config.defaults.position     = "top_right"
naughty.config.defaults.margin       = 8
naughty.config.defaults.gap          = 1
naughty.config.defaults.ontop        = true
naughty.config.defaults.icon_size    = 32
naughty.config.defaults.fg           = beautiful.fg_tooltip
naughty.config.defaults.bg           = beautiful.bg_tooltip
naughty.config.defaults.border_color = beautiful.border_tooltip
naughty.config.defaults.border_width = 2

-- === Screen Setup ===
local function setup_screen(s)
    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

    -- Promptbox
    s.mypromptbox = awful.widget.prompt()

    -- Layoutbox
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
        awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 5, function() awful.layout.inc(1) end),
        awful.button({}, 4, function() awful.layout.inc(-1) end)
    ))

    -- Taglist
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons,
        widget_template = {
            {
                { id = "text_role", widget = wibox.widget.textbox },
                left  = 2, right = 2,
                widget = wibox.container.margin
            },
            id     = "background_role",
            widget = wibox.container.background,
            create_callback = function(self, c3, _, _)
                self:connect_signal("mouse::enter", function()
                    if #c3:clients() > 0 then
                        awesome.emit_signal("bling::tag_preview::update", c3)
                        awesome.emit_signal("bling::tag_preview::visibility", s, true)
                    end
                    if self.bg ~= beautiful.taglist_bg_mouseover then
                        self.bgbackup = self.bg
                        self.fgbackup = self.fg
                        self.has_backup = true
                    end
                    self.bg = beautiful.taglist_bg_mouseover
                    self.fg = beautiful.taglist_fg_mouseover
                end)
                self:connect_signal("mouse::leave", function()
                    awesome.emit_signal("bling::tag_preview::visibility", s, false)
                    if self.has_backup then
                        self.bg = self.bgbackup
                        self.fg = self.fgbackup
                    end
                end)
            end,
        }
    }

    -- Tasklist
    s.mytasklist = awful.widget.tasklist(s, function(c, screen)
        return not awful.widget.tasklist.filter.currenttags(c, screen)
    end, tasklist_buttons)


    -- Wibar
    s.mywibox = awful.wibar({ position = "top", screen = s, height = 16, bg = beautiful.bg_normal, fg = beautiful.fg_focus })
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left Widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle Widget
        { -- Right Widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.imagebox(beautiful.w18),
            wibox.widget.systray(),
            wibox.widget.imagebox(beautiful.w23),
	    wibox.widget.imagebox(beautiful.w22),
            cpu_widget(),
            wibox.widget.imagebox(beautiful.w20),
	    clock_widget(s),
            wibox.widget.imagebox(beautiful.w24),
            logout_menu_widget {
                onreboot  = function() awful.spawn.with_shell("sudo reboot") end,
                onsuspend = function() awful.spawn.with_shell("sudo pm-suspend") end,
                onpoweroff = function() awful.spawn.with_shell("sudo shutdown now") end,
            },
            s.mylayoutbox,
        },
    }
end
awful.screen.connect_for_each_screen(setup_screen)

-- === Bling Setup ===
bling.widget.tag_preview.enable {
    show_client_content = true,
    x = 10,
    y = 25,
    scale = 0.25,
    honor_padding = false,
    honor_workarea = false
}

-- === Mouse Bindings ===
root.buttons(awful.util.table.join(
    awful.button({}, 3, function() mymainmenu:toggle() end),
    awful.button({}, 2, awful.menu.clients),
    awful.button({}, 5, awful.tag.viewnext),
    awful.button({}, 4, awful.tag.viewprev)
))

-- === Keybindings ===
local globalkeys = awful.util.table.join(
    -- Tag Navigation
    awful.key({ modkey }, "Left", awful.tag.viewprev, { description = "view previous", group = "tag" }),
    awful.key({ modkey }, "Right", awful.tag.viewnext, { description = "view next", group = "tag" }),
    awful.key({ modkey }, "Escape", awful.tag.history.restore, { description = "go back", group = "tag" }),

    -- Client Focus and Movement
    awful.key({ modkey }, "j", function() awful.client.focus.byidx(1) end, { description = "focus next by index", group = "client" }),
    awful.key({ modkey }, "k", function() awful.client.focus.byidx(-1) end, { description = "focus previous by index", group = "client" }),
    awful.key({ modkey }, "u", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" }),
    awful.key({ "Mod1" }, "Tab", function()
        for c in awful.client.iterate(function(x) return true end) do
            c.minimized = false
            if not c:isvisible() and c.first_tag then c.first_tag:view_only() end
            client.focus = c
            c:raise()
        end
    end, { description = "alt-tab through all windows", group = "client" }),

    -- Layout Manipulation
    awful.key({ modkey, "Shift" }, "j", function() awful.client.swap.byidx(1) end, { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "k", function() awful.client.swap.byidx(-1) end, { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey, "Control" }, "j", function() awful.screen.focus_relative(1) end, { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey, "Control" }, "k", function() awful.screen.focus_relative(-1) end, { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey }, "l", function() awful.tag.incmwfact(0.05) end, { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey }, "h", function() awful.tag.incmwfact(-0.05) end, { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift" }, "h", function() awful.tag.incnmaster(1, nil, true) end, { description = "increase master clients", group = "layout" }),
    awful.key({ modkey, "Shift" }, "l", function() awful.tag.incnmaster(-1, nil, true) end, { description = "decrease master clients", group = "layout" }),
    awful.key({ modkey, "Control" }, "h", function() awful.tag.incncol(1, nil, true) end, { description = "increase columns", group = "layout" }),
    awful.key({ modkey, "Control" }, "l", function() awful.tag.incncol(-1, nil, true) end, { description = "decrease columns", group = "layout" }),
    awful.key({ modkey }, "space", function() awful.layout.inc(1) end, { description = "select next layout", group = "layout" }),
    awful.key({ modkey, "Shift" }, "space", function() awful.layout.inc(-1) end, { description = "select previous layout", group = "layout" }),

    -- Launcher and Applications
    awful.key({ modkey }, "Return", function() awful.spawn(terminal) end, { description = "open terminal", group = "launcher" }),
    awful.key({ "Control", "Mod1" }, "a", function() awful.spawn("xfce4-terminal") end, { description = "open terminal", group = "launcher" }),
    awful.key({ "Control", "Mod1" }, "d", function() awful.spawn("/usr/local/bin/XDG_CURRENT_DESKTOP-KDE.sh dolphin") end, { description = "open dolphin", group = "launcher" }),
    awful.key({ "Control", "Mod1" }, "e", function() awful.spawn("emacs") end, { description = "open emacs", group = "launcher" }),
    awful.key({ "Control", "Mod1" }, "i", function() awful.spawn("firefox") end, { description = "open firefox", group = "launcher" }),
    awful.key({ "Control", "Mod1" }, "m", function() awful.spawn("thunderbird") end, { description = "open thunderbird", group = "launcher" }),
    awful.key({ "Shift" }, "F12", function() awful.spawn("/usr/local/bin/F12.sh /mnt/stalag13.ici/temp") end, { description = "screenshot", group = "launcher" }),
    
    -- Volume
    awful.key({}, "XF86AudioRaiseVolume",
       function()
	  awful.spawn("wpctl set-mute @DEFAULT_SINK@ 0", false) -- Unmute
	  awful.spawn("wpctl set-volume @DEFAULT_SINK@ 2%+", false)
	  volume_notification()
       end,
       {description = "Increase volume", group = "audio"}
    ),
    awful.key(
       {}, "XF86AudioLowerVolume",
       function()
	  awful.spawn("wpctl set-mute @DEFAULT_SINK@ 0", false) -- Unmute
	  awful.spawn("wpctl set-volume @DEFAULT_SINK@ 2%-", false)
	  volume_notification()
       end,
       {description = "Decrease volume", group = "audio"}
    ),
    awful.key(
       {}, "XF86AudioMute",
       function()
	  awful.spawn("wpctl set-mute @DEFAULT_SINK@ toggle", false)
	  volume_notification()
       end,
       {description = "Toggle mute", group = "audio"}
    ),
    awful.key({}, "XF86AudioPrev", function() awful.spawn("/usr/local/bin/switch-sound") end, { description = "switch sound", group = "launcher" }),
    awful.key({}, "XF86AudioNext", function() awful.spawn("/usr/local/bin/switch-redshift") end, { description = "switch redshift", group = "launcher" }),

    -- Prompt and Restore
    awful.key({ modkey }, "r", function() awful.screen.focused().mypromptbox:run() end, { description = "run prompt", group = "launcher" }),
    awful.key({ modkey }, "x", function()
        awful.prompt.run {
            prompt       = "Run Lua code: ",
            textbox      = awful.screen.focused().mypromptbox.widget,
            exe_callback = awful.util.eval,
            history_path = awful.util.get_cache_dir() .. "/history_eval"
        }
    end, { description = "lua execute prompt", group = "awesome" }),
    awful.key({ modkey }, "p", function() menubar.show() end, { description = "show menubar", group = "launcher" }),
    awful.key({ modkey, "Control" }, "n", function()
        local c = awful.client.restore()
        if c then client.focus = c; c:raise() end
    end, { description = "restore minimized", group = "client" })
)

-- Tag Keybindings (1-9)
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9, function()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then tag:view_only() end
        end, { description = "view tag #" .. i, group = "tag" }),
        awful.key({ modkey, "Control" }, "#" .. i + 9, function()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then awful.tag.viewtoggle(tag) end
        end, { description = "toggle tag #" .. i, group = "tag" }),
        awful.key({ modkey, "Shift" }, "#" .. i + 9, function()
            if client.focus then
                local tag = client.focus.screen.tags[i]
                if tag then client.focus:move_to_tag(tag) end
            end
        end, { description = "move client to tag #" .. i, group = "tag" }),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
            if client.focus then
                local tag = client.focus.screen.tags[i]
                if tag then client.focus:toggle_tag(tag) end
            end
        end, { description = "toggle client on tag #" .. i, group = "tag" })
    )
end

-- Client Keybindings
local clientkeys = awful.util.table.join(
    awful.key({ modkey }, "f", function(c) c.fullscreen = not c.fullscreen; c:raise() end, { description = "toggle fullscreen", group = "client" }),
    awful.key({ "Mod1" }, "F4", function(c) c:kill() end, { description = "close", group = "client" }),
    awful.key({ modkey, "Shift" }, "c", function(c) c:kill() end, { description = "close", group = "client" }),
    awful.key({ modkey, "Control" }, "space", awful.client.floating.toggle, { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end, { description = "move to master", group = "client" }),
    awful.key({ modkey }, "o", function(c) c:move_to_screen() end, { description = "move to screen", group = "client" }),
    awful.key({ modkey }, "t", function(c) c.ontop = not c.ontop end, { description = "toggle keep on top", group = "client" }),
    awful.key({ modkey }, "n", function(c) c.minimized = true end, { description = "minimize", group = "client" }),
    awful.key({ modkey }, "m", function(c) c.maximized = not c.maximized; c:raise() end, { description = "maximize", group = "client" })
)

-- Client Mouse Bindings
local clientbuttons = awful.util.table.join(
    awful.button({}, 1, function(c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize)
)

-- Apply Keybindings
root.keys(globalkeys)

-- === Rules ===
awful.rules.rules = {
    { rule = {},
      properties = {
          border_width = beautiful.border_width,
          border_color = beautiful.border_normal,
          focus = awful.client.focus.filter,
          raise = true,
          keys = clientkeys,
          buttons = clientbuttons,
          screen = awful.screen.preferred,
          placement = awful.placement.no_overlap + awful.placement.no_offscreen
      }
    },
    { rule_any = {
          instance = { "DTA", "copyq" },
          class = { "Arandr", "Gpick", "Kruler", "MessageWin", "Sxiv", "Wpa_gui", "pinentry", "veromix", "xtightvncviewer" },
          name = { "Event Tester" },
          role = { "AlarmWindow", "pop-up" }
      },
      properties = { floating = true }
    },
    { rule_any = { type = { "normal", "dialog" } },
      properties = { titlebars_enabled = true }
    },
    { rule = { class = "thunderbird" },
      properties = { screen = 1, tag = "📩" }
    },
}

-- === Signals ===
client.connect_signal("manage", function(c)
    if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)

client.connect_signal("request::titlebars", function(c)
    local buttons = awful.util.table.join(
        awful.button({}, 1, function() client.focus = c; c:raise(); awful.mouse.client.move(c) end),
        awful.button({}, 3, function() client.focus = c; c:raise(); awful.mouse.client.resize(c) end)
    )
    awful.titlebar(c):setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { align = "center", widget = awful.titlebar.widget.titlewidget(c) },
            buttons = buttons,
            layout = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton(c),
            awful.titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.align.horizontal
    }
end)

client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- === Autostart ===

awful.spawn.single_instance("picom --daemon --backend glx")
awful.spawn.single_instance("pipewire", { stderr_callback = function(err) print("pipewire error: " .. err) end })
awful.spawn.single_instance("pipewire-pulse", { stderr_callback = function(err) print("pipewire-pulse error: " .. err) end })
awful.spawn("pkill wireplumber", false) -- Kill any existing instances
awful.spawn.single_instance("wireplumber", { stderr_callback = function(err) print("wireplumber error: " .. err) end })

awful.spawn("pkill pasystray", false) -- Kill any existing instances
awful.spawn.single_instance("pasystray")
awful.spawn.single_instance("blueman-applet")
awful.spawn("pkill redshift-gtk", false) -- Kill any existing instances
awful.spawn("pkill redshift", false) -- Kill any existing instances
awful.spawn.single_instance("redshift-gtk")

awful.spawn.single_instance("gajim", { tag = "📌" })
awful.spawn.single_instance("signal-desktop", { tag = "📌" })
awful.spawn.single_instance("quiterss", { tag = "9" })
awful.spawn.single_instance("keepassxc", { tag = "9" })
awful.spawn("docftchr.sh")
awful.spawn.single_instance("nextcloud")
awful.spawn.single_instance("thunderbird", { tag = "📩" })
awful.spawn.single_instance("xfce4-power-manager")


-- EOF
