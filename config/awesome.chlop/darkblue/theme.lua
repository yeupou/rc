local theme = {}

-- Paths and Fonts
theme.dir           = os.getenv("HOME") .. "/.config/awesome/darkblue"
theme.font          = "Bitstream Vera Sans 10"
theme.taglist_font  = "Bitstream Vera Bold 10"
theme.tasklist_font = "Bitstream Vera Sans 8"

-- Colors
theme.bg_normal     = "#32302f"
theme.fg_normal     = "#cecece"
theme.bg_focus      = "#32302f"
theme.fg_focus      = "#ffffff"
theme.bg_urgent     = "#C92132"
theme.fg_urgent     = "#282828"
theme.fg_widget     = "#32302f"

theme.taglist_bg_focus     = "#4e9699"
theme.taglist_fg_focus     = "#282828"
theme.taglist_bg_mouseover = "#572aa8"
theme.taglist_bg_occupied  = "#2b5355"
theme.taglist_fg_occupied  = "#929290"
theme.taglist_bg_empty     = "#32302f"
theme.taglist_fg_empty     = "#929290"
theme.taglist_bg_urgent    = "#C92132"
theme.taglist_fg_urgent    = "#929290"

theme.tasklist_bg_normal = "#32302f"
theme.tasklist_fg_normal = "#bebebe"
theme.tasklist_bg_focus  = "#32302f"
theme.tasklist_fg_focus  = "#ffffff"
theme.tasklist_bg_urgent = "#C92132"
theme.tasklist_fg_urgent = "#bebebe"

theme.border_normal = "#32302f"
theme.border_focus  = "#3f3f3f"
theme.border_marked = "#CC9393"

theme.titlebar_bg_normal = "#3f3f3f"
theme.titlebar_fg_normal = "#ffffff"
theme.titlebar_bg_focus  = "#2b5355"
theme.titlebar_fg_focus  = "#ffffff"

theme.green      = "#00b159"
theme.red        = "#d64d4d"
theme.yellow     = "#cc9c00"
theme.blue       = "#428bca"
theme.darkred    = "#c92132"
theme.darkgreen  = "#4d7358"
theme.darkyellow = "#f18e38"
theme.gray       = "#5e5e5e"
theme.violet     = "#8c8ccd"
theme.pink       = "#B85C8A"
theme.black      = theme.bg_normal

-- Borders and Gaps
theme.border_width  = 0
theme.useless_gap   = 4

-- Menu
theme.menu_height       = 16
theme.menu_width        = 250
theme.menu_submenu_icon = theme.dir .. "/icons/submenu.png"
theme.awesome_icon      = theme.dir .. "/icons/awesome.png"

-- Notifications
theme.notification_font         = "Bitstream Vera Sans 13"
theme.notification_bg           = theme.bg_normal
theme.notification_fg           = theme.fg_normal
theme.notification_border_width = 0
theme.notification_border_color = theme.bg_normal
theme.notification_shape        = require("gears").shape.infobubble
theme.notification_opacity      = 0.75
theme.notification_margin       = 30

-- Systray
theme.bg_systray = "#cc9c00"

-- Tag Preview
theme.tag_preview_client_opacity = 1
theme.tag_preview_client_bg      = "#ffffff"
theme.tag_preview_widget_bg      = theme.taglist_bg_mouseover

-- Layout Icons
theme.layout_tile       = theme.dir .. "/icons/layouts/tile.png"
theme.layout_tileleft   = theme.dir .. "/icons/layouts/tileleft.png"
theme.layout_tilebottom = theme.dir .. "/icons/layouts/tilebottom.png"
theme.layout_tiletop    = theme.dir .. "/icons/layouts/tiletop.png"
theme.layout_fairv      = theme.dir .. "/icons/layouts/fairv.png"
theme.layout_fairh      = theme.dir .. "/icons/layouts/fairh.png"
theme.layout_spiral     = theme.dir .. "/icons/layouts/spiral.png"
theme.layout_centerwork = theme.dir .. "/icons/layouts/centerwork.png"
theme.layout_dwindle    = theme.dir .. "/icons/layouts/dwindle.png"
theme.layout_max        = theme.dir .. "/icons/layouts/max.png"
theme.layout_fullscreen = theme.dir .. "/icons/layouts/fullscreen.png"
theme.layout_magnifier  = theme.dir .. "/icons/layouts/magnifier.png"
theme.layout_floating   = theme.dir .. "/icons/layouts/floating.png"
theme.layout_cornernw   = theme.dir .. "/icons/layouts/cornernw.png"
theme.layout_cornerne   = theme.dir .. "/icons/layouts/cornerne.png"

-- Widget Icons
theme.widget_ac          = theme.dir .. "/icons/widgets/ac.png"
theme.widget_battery     = theme.dir .. "/icons/widgets/battery.png"
theme.widget_battery_medium = theme.dir .. "/icons/widgets/battery_medium.png"
theme.widget_battery_low = theme.dir .. "/icons/widgets/battery_low.png"
theme.widget_battery_empty = theme.dir .. "/icons/widgets/battery_empty.png"
theme.widget_battery_no  = theme.dir .. "/icons/widgets/battery_no.png"
theme.widget_mem         = theme.dir .. "/icons/widgets/mem.png"
theme.widget_cpu         = theme.dir .. "/icons/widgets/cpu.png"
theme.widget_temp        = theme.dir .. "/icons/widgets/temp.png"
theme.widget_net         = theme.dir .. "/icons/widgets/net.png"
theme.widget_hdd         = theme.dir .. "/icons/widgets/ssd.png"
theme.widget_clock       = theme.dir .. "/icons/widgets/clock.png"
theme.widget_music       = theme.dir .. "/icons/widgets/note.png"
theme.widget_music_on    = theme.dir .. "/icons/widgets/note_on.png"
theme.widget_music_pause = theme.dir .. "/icons/widgets/pause.png"
theme.widget_music_stop  = theme.dir .. "/icons/widgets/stop.png"
theme.widget_vol         = theme.dir .. "/icons/widgets/vol.svg"
theme.widget_vol_low     = theme.dir .. "/icons/widgets/vol_low.svg"
theme.widget_vol_no      = theme.dir .. "/icons/widgets/vol_no.svg"
theme.widget_vol_mute    = theme.dir .. "/icons/widgets/vol_mute.svg"

-- Titlebar Icons
theme.titlebar_floating_button_focus_active   = theme.dir .. "/icons/titlebar/floating_focus_active.svg"
theme.titlebar_floating_button_normal_active  = theme.titlebar_floating_button_focus_active
theme.titlebar_floating_button_focus_inactive = theme.dir .. "/icons/titlebar/floating_focus_inactive.svg"
theme.titlebar_floating_button_normal_inactive = theme.titlebar_floating_button_focus_inactive

theme.titlebar_maximized_button_focus_active   = theme.dir .. "/icons/titlebar/maximized_focus_active.svg"
theme.titlebar_maximized_button_normal_active  = theme.titlebar_maximized_button_focus_active
theme.titlebar_maximized_button_focus_inactive = theme.dir .. "/icons/titlebar/maximized_focus_inactive.svg"
theme.titlebar_maximized_button_normal_inactive = theme.titlebar_maximized_button_focus_inactive

theme.titlebar_sticky_button_focus_active   = theme.dir .. "/icons/titlebar/sticky_focus_active.svg"
theme.titlebar_sticky_button_normal_active  = theme.titlebar_sticky_button_focus_active
theme.titlebar_sticky_button_focus_inactive = theme.dir .. "/icons/titlebar/sticky_focus_inactive.svg"
theme.titlebar_sticky_button_normal_inactive = theme.titlebar_sticky_button_focus_inactive

theme.titlebar_ontop_button_focus_active   = theme.dir .. "/icons/titlebar/ontop_focus_active.svg"
theme.titlebar_ontop_button_normal_active  = theme.titlebar_ontop_button_focus_active
theme.titlebar_ontop_button_focus_inactive = theme.dir .. "/icons/titlebar/ontop_focus_inactive.svg"
theme.titlebar_ontop_button_normal_inactive = theme.titlebar_ontop_button_focus_inactive

theme.titlebar_close_button_focus  = theme.dir .. "/icons/titlebar/close_focus.svg"
theme.titlebar_close_button_normal = theme.titlebar_close_button_focus

-- Panel Icons
theme.w1  = theme.dir .. "/icons/display/o1.png"
theme.w2  = theme.dir .. "/icons/display/o2.png"
theme.w3  = theme.dir .. "/icons/display/o3.png"
theme.w4  = theme.dir .. "/icons/display/o4.png"
theme.w5  = theme.dir .. "/icons/display/o5.png"
theme.w6  = theme.dir .. "/icons/display/o6.png"
theme.w7  = theme.dir .. "/icons/display/o7.png"
theme.w8  = theme.dir .. "/icons/display/o8.png"
theme.w9  = theme.dir .. "/icons/display/o9.png"
theme.w10 = theme.dir .. "/icons/display/o10.png"
theme.w11 = theme.dir .. "/icons/display/o11.png"
theme.w12 = theme.dir .. "/icons/display/o12.png"
theme.w13 = theme.dir .. "/icons/display/o13.png"
theme.w14 = theme.dir .. "/icons/display/o14.png"
theme.w15 = theme.dir .. "/icons/display/o15.png"
theme.w16 = theme.dir .. "/icons/display/o16.png"
theme.w17 = theme.dir .. "/icons/display/o17.png"
theme.w18 = theme.dir .. "/icons/display/o18.png"
theme.w19 = theme.dir .. "/icons/display/o19.png"
theme.w20 = theme.dir .. "/icons/display/o20.png"
theme.w21 = theme.dir .. "/icons/display/o21.png"
theme.w22 = theme.dir .. "/icons/display/o22.png"
theme.w23 = theme.dir .. "/icons/display/o23.png"
theme.w24 = theme.dir .. "/icons/display/o24.png"

-- Tasklist Settings
theme.tasklist_plain_task_name = true
theme.tasklist_disable_icon    = false

return theme

--  EOF

